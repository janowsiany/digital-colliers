package main

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/janowsiany/digital-colliers/persistence"
	"github.com/janowsiany/digital-colliers/services"
	"github.com/janowsiany/digital-colliers/ui"
)

func main() {
	repository := persistence.NewPaymentsRepository()
	services := services.NewPaymentsService(repository)
	controller := ui.NewPaymentsController(services)

	r := mux.NewRouter()
	r.HandleFunc("/authorize", controller.Authorize).Methods("POST").Headers("Content-Type", "application/json")
	r.HandleFunc("/capture", controller.Capture).Methods("PATCH").Headers("Content-Type", "application/json")
	r.HandleFunc("/void", controller.Void).Methods("PATCH").Headers("Content-Type", "application/json")
	r.HandleFunc("/refund", controller.Refund).Methods("PATCH").Headers("Content-Type", "application/json")
	r.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusMethodNotAllowed)
	})
	http.ListenAndServe(":8000", r)
}
