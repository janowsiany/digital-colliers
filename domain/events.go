package domain

type paymentAuthorized struct {
	amount                            uint64
	currency                          string
	cardNumber, cardVerificationValue string
	cardExpMonth, cardExpYear         uint
}
type paymentCaptured struct {
	amount uint64
}
type paymentVoided struct{}
type paymentRefunded struct {
	amount uint64
}
