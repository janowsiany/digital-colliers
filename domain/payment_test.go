package domain

import (
	"testing"

	"github.com/google/uuid"
)

func TestPayment(t *testing.T) {
	t.Run("requires authorization first", func(t *testing.T) {
		p := NewPayment(uuid.New())

		if err := p.Capture(10); err == nil || err.Error() != "authorize first" {
			t.Error("possible to capture before authorization")
		}
		if err := p.Void(); err == nil || err.Error() != "authorize first" {
			t.Error("possible to void before authorization")
		}
		if err := p.Refund(10); err == nil || err.Error() != "authorize first" {
			t.Error("possible to refund before authorization")
		}
	})

	t.Run("not possible to authorize multiple times", func(t *testing.T) {
		p := NewPayment(uuid.New())
		if err := p.Authorize(100, "PLN", "", "", 0, 0); err != nil {
			t.Error("not possible to authorize once, got:", err)
		}
		if err := p.Authorize(100, "PLN", "", "", 0, 0); err == nil || err.Error() != "already authorized" {
			t.Error("possible to authorize multiple times")
		}
	})

	t.Run("no further action possible after voided", func(t *testing.T) {
		p := NewPayment(uuid.New())
		if err := p.Authorize(100, "PLN", "", "", 0, 0); err != nil {
			t.Error("not possible to authorize once, got:", err)
		}
		if err := p.Void(); err != nil {
			t.Error("not possible to void once, got:", err)
		}
		if err := p.Refund(10); err == nil || err.Error() != "already voided" {
			t.Error("possible to refund after voided")
		}
		if err := p.Capture(10); err == nil || err.Error() != "already voided" {
			t.Error("possible to capture after voided")
		}
		if err := p.Void(); err == nil || err.Error() != "already voided" {
			t.Error("possible to void multiple times")
		}
	})

	t.Run("not possible to void after capture", func(t *testing.T) {
		p := NewPayment(uuid.New())
		if err := p.Authorize(100, "PLN", "", "", 0, 0); err != nil {
			t.Error("not possible to authorize once, got:", err)
		}
		if err := p.Capture(10); err != nil {
			t.Error("not possible to capture once, got:", err)
		}
		if err := p.Void(); err == nil || err.Error() != "cannot void at this stage" {
			t.Error("not possible to refund once, got:", err)
		}
	})

	t.Run("not possible to capture after refund", func(t *testing.T) {
		p := NewPayment(uuid.New())
		if err := p.Authorize(100, "PLN", "", "", 0, 0); err != nil {
			t.Error("not possible to authorize once, got:", err)
		}
		if err := p.Capture(10); err != nil {
			t.Error("not possible to capture once, got:", err)
		}
		if err := p.Refund(5); err != nil {
			t.Error("not possible to refund once, got:", err)
		}
		if err := p.Capture(5); err == nil || err.Error() != "already refunded" || p.captured != 5 {
			t.Error("possible to capture after refund")
		}
	})

	t.Run("not possible to capture more than authorized", func(t *testing.T) {
		p := NewPayment(uuid.New())
		if err := p.Authorize(100, "PLN", "", "", 0, 0); err != nil {
			t.Error("not possible to authorize once, got:", err)
		}
		if err := p.Capture(20); err != nil {
			t.Error("not possible to capture once, got:", err)
		}
		if err := p.Capture(30); err != nil {
			t.Error("not possible to capture twice, got:", err)
		}
		if err := p.Capture(50); err != nil {
			t.Error("not possible to capture thrice, got:", err)
		}
		if err := p.Capture(1); err == nil || err.Error() != "not enough money authorized" || p.captured != p.amount {
			t.Error("possible to capture more than authorized")
		}
	})

	t.Run("not possible to refund more than captured", func(t *testing.T) {
		p := NewPayment(uuid.New())
		if err := p.Authorize(100, "PLN", "", "", 0, 0); err != nil {
			t.Error("not possible to authorize once, got:", err)
		}
		if err := p.Capture(50); err != nil {
			t.Error("not possible to capture once, got:", err)
		}
		if err := p.Refund(27); err != nil {
			t.Error("not possible to refund once, got:", err)
		}
		if err := p.Refund(23); err != nil {
			t.Error("not possible to refund once, got:", err)
		}
		if err := p.Refund(1); err == nil || err.Error() != "not enough money captured" || p.captured != 0 {
			t.Error("possible to refund more than captured", err)
		}
	})
}
