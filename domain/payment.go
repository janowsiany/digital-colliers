package domain

import (
	"errors"

	"github.com/google/uuid"
)

type Payment struct {
	uuid                         uuid.UUID
	authorized, voided, refunded bool
	amount, captured             uint64
	cardNumber                   string
	currency                     string

	changelog []interface{}
}

func NewPayment(id uuid.UUID) *Payment {
	return &Payment{uuid: id}
}

func (p *Payment) Authorize(amount uint64, currency, cardNumber, cardVerificationValue string, cardExpMonth, cardExpYear uint) error {
	return p.raise(
		&paymentAuthorized{
			amount:                amount,
			currency:              currency,
			cardNumber:            cardNumber,
			cardVerificationValue: cardVerificationValue,
			cardExpMonth:          cardExpMonth,
			cardExpYear:           cardExpYear,
		},
	)
}

func (p *Payment) Capture(amount uint64) error {
	return p.raise(&paymentCaptured{amount})
}

func (p *Payment) Void() error {
	return p.raise(&paymentVoided{})
}

func (p *Payment) Refund(amount uint64) error {
	return p.raise(&paymentRefunded{amount})
}

func (p *Payment) Apply(e interface{}) error {
	switch e := e.(type) {
	case *paymentAuthorized:
		return p.handleAuthorized(e)
	case *paymentCaptured:
		return p.handleCaptured(e)
	case *paymentVoided:
		return p.handleVoided(e)
	case *paymentRefunded:
		return p.handleRefunded(e)
	default:
		return errors.New("unknown event")
	}
}

func (p *Payment) UUID() uuid.UUID {
	return p.uuid
}

func (p *Payment) Amount() uint64 {
	return p.amount
}

func (p *Payment) Captured() uint64 {
	return p.captured
}

func (p *Payment) Currency() string {
	return p.currency
}

func (p *Payment) Changelog() []interface{} {
	return p.changelog
}

func (p *Payment) Flush() {
	p.changelog = nil
}

func (p *Payment) handleAuthorized(e *paymentAuthorized) error {
	if e.cardNumber == "4000000000000119" {
		return errors.New("authorize failure")
	}
	if p.authorized {
		return errors.New("already authorized")
	}
	p.authorized = true
	p.cardNumber = e.cardNumber
	p.amount = e.amount
	p.currency = e.currency
	return nil
}

func (p *Payment) handleCaptured(e *paymentCaptured) error {
	if p.cardNumber == "4000000000000259" {
		return errors.New("capture failure")
	}
	if !p.authorized {
		return errors.New("authorize first")
	}
	if p.voided {
		return errors.New("already voided")
	}
	if p.refunded {
		return errors.New("already refunded")
	}
	if p.captured+e.amount > p.amount {
		return errors.New("not enough money authorized")
	}
	p.captured += e.amount
	return nil
}

func (p *Payment) handleRefunded(e *paymentRefunded) error {
	if p.cardNumber == "4000000000003238" {
		return errors.New("refund failure")
	}
	if !p.authorized {
		return errors.New("authorize first")
	}
	if p.voided {
		return errors.New("already voided")
	}
	if e.amount > p.captured {
		return errors.New("not enough money captured")
	}

	p.refunded = true
	p.captured -= e.amount

	return nil
}

func (p *Payment) handleVoided(e *paymentVoided) error {
	if !p.authorized {
		return errors.New("authorize first")
	}

	if p.voided {
		return errors.New("already voided")
	}

	if p.captured > 0 || p.refunded {
		return errors.New("cannot void at this stage")
	}

	p.voided = true

	return nil
}

func (p *Payment) raise(e interface{}) error {
	if err := p.Apply(e); err != nil {
		return err
	}
	p.changelog = append(p.changelog, e)
	return nil
}
