package services

import (
	"errors"
	"log"
)

var (
	ErrPaymentNotFound          = errors.New("payment not found")
	ErrNotEnoughMoneyAuthorized = errors.New("not enough money authorized")
	ErrNotEnoughMoneyCaptured   = errors.New("not enough money captured")
	ErrPaymentVoided            = errors.New("payment is already voided")
	ErrPaymentRefunded          = errors.New("payment is already refunded")
	ErrWrongCreditCardNumber    = errors.New("invalid credit card number")
	ErrVoidForbidden            = errors.New("cannot void at this stage")
	ErrUnknown                  = errors.New("unexpected error occured")
)

func mapDomainError(err error) error {
	switch err.Error() {
	case "authorize first":
		return ErrPaymentNotFound
	case "authorize failure", "capture failure", "refund failure":
		return ErrWrongCreditCardNumber
	case "already voided":
		return ErrPaymentVoided
	case "already refunded":
		return ErrPaymentRefunded
	case "not enough money authorized":
		return ErrNotEnoughMoneyAuthorized
	case "not enough money captured":
		return ErrNotEnoughMoneyCaptured
	case "cannot void at this stage":
		return ErrVoidForbidden
	default:
		log.Println("unknown domain error", err)
		return ErrUnknown
	}
}
