package services

import (
	"github.com/google/uuid"
	"github.com/janowsiany/digital-colliers/domain"
	"github.com/janowsiany/digital-colliers/persistence"
)

type PaymentsService struct {
	repository *persistence.PaymentsRepository
}

func NewPaymentsService(r *persistence.PaymentsRepository) *PaymentsService {
	return &PaymentsService{r}
}

func (s *PaymentsService) Authorize(amount uint64, currency, cardNumber, cardVerificationValue string, cardExpMonth, cardExpYear uint) (*domain.Payment, error) {
	p := domain.NewPayment(uuid.New())
	if err := p.Authorize(amount, currency, cardNumber, cardVerificationValue, cardExpMonth, cardExpYear); err != nil {
		return nil, mapDomainError(err)
	}
	s.repository.Save(p)
	return p, nil
}

func (s *PaymentsService) Capture(id uuid.UUID, amount uint64) (*domain.Payment, error) {
	p, err := s.repository.Load(id)
	if err != nil {
		return nil, mapDomainError(err)
	}
	if err := p.Capture(amount); err != nil {
		return nil, mapDomainError(err)
	}
	s.repository.Save(p)
	return p, nil
}

func (s *PaymentsService) Void(id uuid.UUID) (*domain.Payment, error) {
	p, err := s.repository.Load(id)
	if err != nil {
		return nil, mapDomainError(err)
	}
	if err := p.Void(); err != nil {
		return nil, mapDomainError(err)
	}
	s.repository.Save(p)
	return p, nil
}

func (s *PaymentsService) Refund(id uuid.UUID, amount uint64) (*domain.Payment, error) {
	p, err := s.repository.Load(id)
	if err != nil {
		return nil, mapDomainError(err)
	}
	if err := p.Refund(amount); err != nil {
		return nil, mapDomainError(err)
	}
	s.repository.Save(p)
	return p, nil
}
