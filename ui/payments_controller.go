package ui

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"regexp"

	"github.com/google/uuid"
	"github.com/janowsiany/digital-colliers/services"
)

type PaymentsController struct {
	service *services.PaymentsService
}

var CreditCardRegex = regexp.MustCompile("^[0-9]{8,16}$")
var CardVerificationValueRegex = regexp.MustCompile("^[0-9]{3}$")

func NewPaymentsController(s *services.PaymentsService) *PaymentsController {
	return &PaymentsController{s}
}

func (c *PaymentsController) Authorize(w http.ResponseWriter, r *http.Request) {
	req := &authorizeRequest{}
	if err := c.unmarshal(r, req); err != nil {
		c.sendError(w, err)
		return
	}

	if req.Amount == 0 {
		c.sendError(w, ErrInvalidAmount)
		return
	}

	if req.Currency == "" {
		c.sendError(w, ErrInvalidCurreny)
		return
	}

	if !CreditCardRegex.MatchString(req.CardNumber) {
		c.sendError(w, ErrInvalidCardNumber)
		return
	}

	if !CardVerificationValueRegex.MatchString(req.CardVerificationValue) {
		c.sendError(w, ErrInvalidCardVerificationValue)
		return
	}

	if req.CardExpMonth < 1 || req.CardExpMonth > 12 {
		c.sendError(w, ErrInvalidCardExpMonth)
		return
	}

	if req.CardExpYear < 1970 {
		c.sendError(w, ErrInvalidCardExpYear)
		return
	}

	p, err := c.service.Authorize(req.Amount, req.Currency, req.CardNumber, req.CardVerificationValue, req.CardExpMonth, req.CardExpYear)
	if err != nil {
		c.sendError(w, err)
		return
	}

	if err := c.sendResponse(w, &authorizeResponse{Id: p.UUID(), Currency: p.Currency(), Amount: p.Amount()}); err != nil {
		c.sendError(w, err)
	}
}

func (c *PaymentsController) Capture(w http.ResponseWriter, r *http.Request) {
	req := &captureRequest{}
	if err := c.unmarshal(r, req); err != nil {
		c.sendError(w, err)
		return
	}

	if req.Id == uuid.Nil {
		c.sendError(w, ErrEmptyId)
		return
	}

	if req.Amount == 0 {
		c.sendError(w, ErrInvalidAmount)
		return
	}

	p, err := c.service.Capture(req.Id, req.Amount)
	if err != nil {
		c.sendError(w, err)
		return
	}

	if err := c.sendResponse(w, &captureResponse{Currency: p.Currency(), Amount: p.Amount() - p.Captured()}); err != nil {
		c.sendError(w, err)
	}
}

func (c *PaymentsController) Void(w http.ResponseWriter, r *http.Request) {
	req := &voidRequest{}
	if err := c.unmarshal(r, req); err != nil {
		c.sendError(w, err)
		return
	}

	if req.Id == uuid.Nil {
		c.sendError(w, ErrEmptyId)
		return
	}

	p, err := c.service.Void(req.Id)
	if err != nil {
		c.sendError(w, err)
		return
	}

	if err := c.sendResponse(w, &voidResponse{Currency: p.Currency(), Amount: 0}); err != nil {
		c.sendError(w, err)
	}
}

func (c *PaymentsController) Refund(w http.ResponseWriter, r *http.Request) {
	req := &refundRequest{}
	if err := c.unmarshal(r, req); err != nil {
		c.sendError(w, err)
		return
	}

	if req.Id == uuid.Nil {
		c.sendError(w, ErrEmptyId)
		return
	}

	if req.Amount == 0 {
		c.sendError(w, ErrInvalidAmount)
		return
	}

	p, err := c.service.Refund(req.Id, req.Amount)
	if err != nil {
		c.sendError(w, err)
		return
	}

	if err := c.sendResponse(w, &refundResponse{Currency: p.Currency(), Amount: p.Captured()}); err != nil {
		c.sendError(w, err)
	}
}

func (c *PaymentsController) unmarshal(r *http.Request, target interface{}) error {
	reqBody, err := io.ReadAll(r.Body)
	if err != nil {
		return fmt.Errorf("failed to read request body: %w", err)
	}
	if len(reqBody) == 0 {
		return ErrrEmptyRequestBody
	}

	if err := json.Unmarshal(reqBody, target); err != nil {
		return fmt.Errorf("failed to unmarshal request body: %w", err)
	}

	return nil
}

func (c *PaymentsController) sendResponse(w http.ResponseWriter, response interface{}) error {
	bytes, err := json.Marshal(response)
	if err != nil {
		return fmt.Errorf("failed to marshal response body: %w", err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(bytes)

	return nil
}

func (c *PaymentsController) sendError(w http.ResponseWriter, err error) {
	w.Header().Set("Content-Type", "application/json")
	switch err {
	case services.ErrPaymentNotFound:
		w.WriteHeader(http.StatusNotFound)
	case ErrrEmptyRequestBody, ErrEmptyId, ErrInvalidAmount, ErrInvalidCurreny, ErrInvalidCardNumber, ErrInvalidCardVerificationValue, ErrInvalidCardExpMonth, ErrInvalidCardExpYear:
		w.WriteHeader(http.StatusUnprocessableEntity)
	case services.ErrNotEnoughMoneyAuthorized, services.ErrNotEnoughMoneyCaptured, services.ErrWrongCreditCardNumber:
		w.WriteHeader(http.StatusUnprocessableEntity)
	case services.ErrPaymentVoided, services.ErrPaymentRefunded, services.ErrVoidForbidden:
		w.WriteHeader(http.StatusConflict)
	case services.ErrUnknown:
		w.WriteHeader(http.StatusInternalServerError)
	default:
		log.Println("unknown error", err)
		err = ErrUnknown
		w.WriteHeader(http.StatusInternalServerError)
	}
	w.Write([]byte(fmt.Sprintf(`{ "error": "%s" }`, err)))
}
