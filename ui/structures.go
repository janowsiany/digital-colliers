package ui

import "github.com/google/uuid"

type authorizeRequest struct {
	Amount                uint64 `json:"amount"`
	Currency              string `json:"currency"`
	CardNumber            string `json:"card_number"`
	CardVerificationValue string `json:"cvv"`
	CardExpMonth          uint   `json:"card_exp_month"`
	CardExpYear           uint   `json:"card_exp_year"`
}
type captureRequest struct {
	Id     uuid.UUID `json:"id"`
	Amount uint64    `json:"amount"`
}
type voidRequest struct {
	Id uuid.UUID `json:"id"`
}
type refundRequest struct {
	Id     uuid.UUID `json:"id"`
	Amount uint64    `json:"amount"`
}

type authorizeResponse struct {
	Id       uuid.UUID `json:"id"`
	Amount   uint64    `json:"amount"`
	Currency string    `json:"currency"`
}
type captureResponse struct {
	Amount   uint64 `json:"amount"`
	Currency string `json:"currency"`
}
type voidResponse struct {
	Amount   uint64 `json:"amount"`
	Currency string `json:"currency"`
}
type refundResponse struct {
	Amount   uint64 `json:"amount"`
	Currency string `json:"currency"`
}
