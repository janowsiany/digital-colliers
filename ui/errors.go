package ui

import "errors"

var (
	ErrrEmptyRequestBody            = errors.New("request body must not be empty")
	ErrUnknown                      = errors.New("unexpected error occured")
	ErrEmptyId                      = errors.New("`id` must not be empty")
	ErrInvalidAmount                = errors.New("`amount` must provided and greater or equal 1")
	ErrInvalidCurreny               = errors.New("`currency` must not be empty")
	ErrInvalidCardNumber            = errors.New("`card_number` must contain 8 to 16 digits")
	ErrInvalidCardVerificationValue = errors.New("`cvv` must contain 3 digits")
	ErrInvalidCardExpMonth          = errors.New("`card_exp_month` must be in range from 1 to 12")
	ErrInvalidCardExpYear           = errors.New("`card_exp_year` must be greater or equal 1970")
)
