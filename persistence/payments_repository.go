package persistence

import (
	"sync"

	"github.com/google/uuid"

	"github.com/janowsiany/digital-colliers/domain"
)

type PaymentsRepository struct {
	sync.Mutex
	eventStreams map[uuid.UUID][]interface{}
}

func NewPaymentsRepository() *PaymentsRepository {
	return &PaymentsRepository{eventStreams: make(map[uuid.UUID][]interface{})}
}

func (r *PaymentsRepository) Load(id uuid.UUID) (*domain.Payment, error) {
	r.Lock()
	defer r.Unlock()

	p := domain.NewPayment(id)

	for _, e := range r.eventStreams[id] {
		if err := p.Apply(e); err != nil {
			return nil, err
		}
	}

	return p, nil
}

func (r *PaymentsRepository) Save(p *domain.Payment) {
	r.Lock()
	defer r.Unlock()

	r.eventStreams[p.UUID()] = append(r.eventStreams[p.UUID()], p.Changelog()...)
	p.Flush()
}
